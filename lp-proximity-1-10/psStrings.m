function psStrings()
    for i = 1:10
        load(strcat('results_LTM',int2str(i),'.mat'));
        app_tot_ps = LTM_all_ep(:,4,:);
        sum_tot_ps = sum(app_tot_ps, 1);
        newsum_tot_ps = reshape(sum_tot_ps,[1,100]);
        count_tot_ps = sum(app_tot_ps~=0,1);
        newcount_tot_ps = reshape(count_tot_ps,[1,100]);
        mean_tot_ps = newsum_tot_ps./newcount_tot_ps;
        mean_tot_ps(isnan(mean_tot_ps)) = 0;
        allOneString = sprintf('%.2f,' , mean_tot_ps);
        s = allOneString(1:end-1);
        disp(strcat('ps.append([',s,'])'));
    end
end